﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingBoard
{
    static class Program
    {

        [STAThread]
        static void Main()
        {
            //  Form1.Size = new System.Drawing.Size(100, 100);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Form1 form = new Form1();
            form.Size = new System.Drawing.Size(1366, 768);
            Application.Run(form);
        }


        static void PutPixel(Graphics g, int x, int y, Color c)
        {
            Bitmap bm = new Bitmap(1, 1);
            bm.SetPixel(0, 0, Color.Red);
            g.DrawImageUnscaled(bm, x, y);
        }


        public static void paint(PaintEventArgs e)
        {
            Graphics myGraphics = e.Graphics;
            DrawMeLikeAFrenchGirl(myGraphics);
            myGraphics.Dispose();
        }

        public static void DrawMeLikeAFrenchGirl(Graphics myGraphics)
        {
            circle(myGraphics);
        }

        public static void circle(Graphics myGraphics)
        {
            double radius = 5;
            for (int j = 1; j <= 25; j++)
            {
                radius = (j + 1) * 5;
                for (double i = 0.0; i < 360.0; i += 0.1)
                {
                    double angle = i * System.Math.PI / 180;
                    int x = (int)(150 + radius * System.Math.Cos(angle));
                    int y = (int)(150 + radius * System.Math.Sin(angle));

                    PutPixel(myGraphics, x, y, Color.Red);
                    //System.Threading.Thread.Sleep(1); // If you want to draw circle very slowly.
                }
            }
        }
    }
}
